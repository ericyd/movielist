const getUniqueMovies = allTheaters => {
  return (
    allTheaters
      // returns an array of arrays because theaterDetails.movies are each arrays
      .map(theaterDetails => theaterDetails.movies)
      // since theaterDetails.movies is an array,
      // they must be concatenated before further
      // mapping can occur
      .reduce((prev, curr) => Array.prototype.concat(prev ? prev : [], curr))
      // all we need is the titles right now
      .map(theaterShowings => theaterShowings.title)
      // this filter has it's drawbacks but will be fine for this implementations
      // https://stackoverflow.com/questions/9229645/remove-duplicates-from-javascript-array
      .filter((title, pos, self) => {
        return self.indexOf(title) === pos;
      })
  );
};

const isMovieShowingAtTheater = (theater, movie) => {
  if (!theater.hasOwnProperty('movies') || !Array.isArray(theater.movies))
    return false;
  const theaterMovieTitles = theater.movies.map(movie => movie.title);
  return theaterMovieTitles.includes(movie);
};

const getTheaterShowtimesForMovie = (theater, movieTitle) => {
  if (isMovieShowingAtTheater(theater, movieTitle)) {
    // if no times are listed, return an empty array
    let times = theater.movies.filter(movie => movie.title === movieTitle)[0].times;
    if (times === undefined || times === null) {
      times = [];
    }
    return {
      theater: theater.theaterName,
      times: times
    };
  }
};

const getAllShowtimesForMovie = (theaterList, movieTitle) => {
  return theaterList
    .map(theater => {
      return getTheaterShowtimesForMovie(theater, movieTitle);
    })
    .filter(showtimes => {
      return showtimes !== undefined && showtimes !== null;
    });
};

const getMovieDetails = (theatersList, showtimes, movieTitle) => {
  const relevantTheater = theatersList
    // get only the theater that matches the first showtime
    .filter(theater => theater.theaterName === showtimes[0].theater)
    // get the movies list for that theater
    // since movies is an array, this will return an array of arrays with only one item,
    // so just take the first one
    .map(theater => theater.movies)[0]
    // get only the movie that matches the name of the movieTitle argument
    .filter(movies => movies.title === movieTitle)[0];

  return {
    runtime: relevantTheater.runtime,
    link: relevantTheater.link,
    imageSrc: relevantTheater.imageSrc
  };
};

const processResults = theatersList => {
  return new Promise((resolve, reject) => {
    const uniqueTitles = getUniqueMovies(theatersList);
    const movies = uniqueTitles.map(title => {
      const showtimes = getAllShowtimesForMovie(theatersList, title);
      const movie = getMovieDetails(theatersList, showtimes, title);
      return {
        title: title,
        runtime: movie.runtime,
        link: movie.link,
        imageSrc: movie.imageSrc,
        showtimes: showtimes
      };
    });
    resolve(movies);
  });
};

module.exports = {
  transformData: processResults,
  getMovieDetails: getMovieDetails,
  getAllShowtimesForMovie: getAllShowtimesForMovie,
  getTheaterShowtimesForMovie: getTheaterShowtimesForMovie,
  isMovieShowingAtTheater: isMovieShowingAtTheater,
  getUniqueMovies: getUniqueMovies
};
