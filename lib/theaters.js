module.exports = [
  {
    id: 9966,
    title: 'Academy-Theatre-Showtimes',
    displayName: 'Academy Theater'
  },
  {
    id: 5292,
    title: 'Avalon-Theatre-_-Portland-Showtimes',
    displayName: 'Avalon Theater'
  },
  {
    id: 2480,
    title: 'Cine-Magic-Theatre-Showtimes',
    displayName: 'Cine-Magic Theater'
  },
  {
    id: 2488,
    title: 'Clinton-Street-Theater-Showtimes',
    displayName: 'Clinton Street Theater'
  },
  {
    id: 5316,
    title: 'Century-Eastport-16-Showtimes',
    displayName: 'Century Eastport 16'
  },
  {
    id: 7547,
    title: 'Regal-Fox-Tower-Stadium-10-Showtimes',
    displayName: 'Regal Fox Tower'
  },
  {
    id: 2432,
    title: 'Hollywood-Theatre-_-Portland-Showtimes',
    displayName: 'Hollywood Theater'
  },
  {
    id: 3632,
    title: 'McMenamins-Kennedy-School-Theatre-Showtimes',
    displayName: 'McMenamins Kennedy School'
  },
  {
    id: 2481,
    title: 'Laurelhurst-Theatre-Showtimes',
    displayName: 'Laurelhurst Theater'
  },
  {
    id: 10393,
    title: 'Living-Room-Theaters-_-Portland-Showtimes',
    displayName: 'Living Room Theaters'
  },
  {
    id: 2434,
    title: 'Regal-Lloyd-Center-10-and-IMAX-Showtimes',
    displayName: 'Regal Lloyd Center'
  },
  {
    id: 2482,
    title: 'Moreland-Theatre-Showtimes',
    displayName: 'Moreland Theater'
  },
  {
    id: 8534,
    title: 'Regal-Pioneer-Place-Stadium-6-Showtimes',
    displayName: 'Regal Pioneer Place'
  },
  {
    id: 5612,
    title: 'Fifth-Avenue-Cinemas-Showtimes',
    displayName: 'Fifth Avenue Cinemas (PSU)'
  },
  {
    id: 2500,
    title: 'Mt.-Tabor-Theater-Showtimes',
    displayName: 'Mt. Tabor Theater'
  }
];
