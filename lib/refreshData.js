const theaters = require('./theaters');
const fetchTheater = require('./util/fetchTheater');
const promisifyArray = require('./util/promisifyArray');
const processResults = require('./processResults').transformData;
const saveAll = require('../models/saveAll');
const getAll = require('../models/getAll');
const saveOne = require('../models/saveOne');

/**
 * Get movies, extract theater data, and transform into movies list
 * @param {FirebaseAdmin} firebaseAdmin 
 */
function refreshData(firebaseAdmin) {
  const fetchAllTheaters = promisifyArray(theaters, fetchTheater);
  return (
    Promise.all(fetchAllTheaters)
      // save theater data in case something else breaks
      .then(res => {
        // res[].data contains the theater schemas
        const theaterData = res
          .map(r => r.data)
          // remove theaters with no movies playing
          .filter(theater => theater.movies.length > 0);

        saveAll(firebaseAdmin, 'theaters', theaterData);
        return processResults(theaterData);
      })
      // save movies
      .then(movies => {
        if (movies.length > 0) {
          saveAll(firebaseAdmin, 'movies', movies);
          saveOne(firebaseAdmin, 'lastUpdated', {
            timestamp: new Date().getTime()
          });
        }
        return new Promise((resolve, reject) => {
          if (movies.length > 0) {
            resolve(movies);
          } else {
            reject('issue retrieving data');
          }
        });
      })
      .catch(err => {
        console.log(err);
      })
  );
}

module.exports = refreshData;
