const cheerio = require('cheerio');

/**
 * @param {string} body - an HTML body of a response
 */
function extractMovieData(body) {
  const $ = cheerio.load(body);
  const theaterName = $('.theatre-header .name').text();
  const movieListings = $('.info');
  const movies = [];

  // extract movie data for each movie in the theater and save to the movies object
  movieListings.each(function(i, movieListing) {
    // for cheerio/jquery each,
    // use $(this) to select the elements instead of the element passed in the callback
    let imageSrc = $(this).parent().find('a img').eq(0).attr('src');
    let title = $(this).find('.name');
    let titleLink = 'http://quicklookfilms.com' + title.attr('href');
    let titleText = title.text();
    let runtime = $(this).find('.runtime').text().slice(3);
    let times = [];
    // if the theater has a "buy now" option the markup for the show times is different
    if ($(this).find('.showtime').toArray().length === 0) {
      times = $(this)
        .find('.times a')
        .toArray()
        .map(el => $(el).text().replace('Buy Now', ''));
    } else {
      times = $(this).find('.showtime').toArray().map(el => $(el).text());
    }
    movies.push({
      title: titleText,
      imageSrc: imageSrc,
      link: titleLink,
      times: times,
      runtime: runtime
    });
  }, this);

  return {
    theaterName: theaterName,
    movies: movies,
    rawHTML: body,
    dateModified: new Date()
  };
}

module.exports = extractMovieData;
