const axios = require('axios');
const formatDate = require('./formatDate');
const parseTheaterHTML = require('./parseTheaterHTML');

/**
 * 
 * @param {number} theaterID - the id of the theater
 * @param {string} theaterTitle - the title of the theater
 * @param {Date} date 
 */
const requestTheater = (theater, date = new Date()) => {
  const [theaterID, theaterTitle] = [theater.id, theater.title];
  const formattedDate = formatDate(date);
  const url = `http://quicklookfilms.com/includes/ajax_times/${theaterID}/${theaterTitle}/${formattedDate}/`;
  return axios.get(url, {
    transformResponse: function(data) {
      return parseTheaterHTML(data);
    }
  });
};

module.exports = requestTheater;
