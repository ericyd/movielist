/**
 * convert a normal array into an array of promise
 * @param {array} array
 * @param {function} promise
 * @return {array} of promise
 */

module.exports = function promisifyArray(array, promise) {
  if (!Array.isArray(array)) throw new Error('first argument is not an array');
  return array.map(unit => promise(unit));
};
