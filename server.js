//
// Dependencies
//
require('dotenv').config();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const router = express.Router();
const firebase = require('firebase');
const refreshData = require('./lib/refreshData');
const getAll = require('./models/getAll');
const getOne = require('./models/getOne');
const theaters = require('./lib/theaters');

//
// Set up server
//
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('port', process.env.PORT || 5000);
// serves all files in public as static files
app.use(express.static(__dirname + '/public'));
// since public/index.html looks for ../dist/index.js,
// giving the dist files the path prefix of dist ensures that they can be found.
app.use('/dist', express.static('dist'));

//
// Initialize Firebase
//
const firebaseAdmin = firebase.initializeApp({
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_URL,
  storageBucket: process.env.STORAGE_BUCKET,
  projectId: process.env.PROJECT_ID,
  messagingSenderId: process.env.MESSAGING_SENDER_ID
});

// sign in so data can be saved to the db
const auth = firebaseAdmin.auth();
auth
  .signInWithEmailAndPassword(process.env.EMAIL, process.env.PASSWORD)
  .then(user => {
    // console.log(auth.currentUser);
  })
  .catch(err => {
    console.log(err);
  });

//
// Set up routes
//
// route() will allow you to use same path for different HTTP operation.
// So if you have same URL but with different HTTP OP such as POST,GET etc
// Then use route() to remove redundant code.

app.get('/', function(request, response) {
  response.render('index.html');
});

// get all movies
router.route('/movies').get(function(req, res) {
  getAll(firebaseAdmin, 'movies')
    .then(movies => {
      res.json({ error: false, movies: movies, message: 'got movies' });
    })
    .catch(err => {
      res.json({ message: 'error getting data', movies: null, error: err });
    });
});

// get all theaters
router.route('/theaters').get(function(req, res) {
  res.json(theaters);
});

// fetch html from quicklookfilms and process into new movies list
// save updated data to the database
router.route('/refresh').get(function(req, res) {
  refreshData(firebaseAdmin)
    .then(
      data => {
        if (data !== undefined && data !== null) {
          res.json({ error: false, message: 'Refreshed', movies: data });
        } else {
          res.json({
            error: false,
            message: 'No data returned from firebase',
            movies: data
          });
        }
      },
      err => {
        res.json({ error: true, message: err });
      }
    )
    .catch(err => {
      res.json({ error: true, message: err });
    });
});

// fetch html from quicklookfilms and process into new movies list
// save updated data to the database
router.route('/last-updated').get(function(req, res) {
  getOne(firebaseAdmin, 'lastUpdated')
    .then(
      response => {
        const data = response[Object.keys(response)[0]];
        res.json({
          error: false,
          message: 'Refreshed',
          lastUpdated: data.timestamp
        });
      },
      err => {
        res.json({ error: true, message: err });
      }
    )
    .catch(err => {
      res.json({ error: true, message: err });
    });
});

app.use('/', router);

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
