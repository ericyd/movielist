const timestampToDate = require('./timestampToDate');

/**
 * @param {string} showtime - in format HH:MM pm, e.g. 12:12 pm
 * @param {date} now - the time to compare to
 */
module.exports = function(showtime, now = new Date()) {
  const then = timestampToDate(showtime);
  return then < now;
};
