/**
 * converts a string timestamp to a date (today) with the correct timestamp
 * @param {string} timestamp format examples: '12:33 am', '7:10 pm'
 */
const timestampToDate = (timestamp) => {
  const date = new Date();
  const m = timestamp.slice(-2);
  let [hours, minutes] = timestamp.slice(0, -2).trim().split(':');
  hours = m === 'pm' && Number(hours) !== 12
    ? Number(hours) + 12
    : Number(hours);
  date.setHours(hours, Number(minutes));
  return date;
}

module.exports = timestampToDate;