/**
 * sorts two objects passed on the passed property
 * a and b must have the same properties
 * @param {string} property
 * @param {object} a
 * @param {object} b
 */
const sortObjectsByProp = (property, a, b) => {
  // pseudo-curry it if only property is passed
  if (a === undefined && b === undefined) {
    return function (a, b) {
      if (a[property] < b[property])
          return -1;
      if (a[property] > b[property])
          return 1;
      return 0;
    }
  }

  // otherwise just evaluate
  if (a[property] < b[property])
      return -1;
  if (a[property] > b[property])
      return 1;
  return 0;
}

module.exports = sortObjectsByProp;