const isShowtimeInPast = require('./isShowtimeInPast');

/**
 * checks every value in an array to see if it is in past
 * @param {array} times
 * @return {boolean}
 */
function isEveryShowtimeInPast(times) {
  if (times === undefined || times === null) return true;
  const timesInFuture = times.filter(time => {
    return !isShowtimeInPast(time);
  });

  return timesInFuture.length <= 0;
}

module.exports = isEveryShowtimeInPast;
