import App from './components/App';
import MovieList from './components/MovieList';
import TheaterList from './components/TheaterList';

const app = new App({
  target: document.getElementById('app'),
  data: {
    views: [
      {
        id: 'movies',
        title: 'Movies',
        component: MovieList
      },
      {
        id: 'theaters',
        title: 'Theaters',
        component: TheaterList
      }
    ],
    // must match the id of one of the views
    active: 'movies'
  },
  components: {
    TheaterList,
    MovieList
  }
});
