/**
 * Get all records from supplied path
 * @param {FirebaseAdmin} firebaseAdmin must already be initialized
 * @param {string} path either 'movies' or 'theaters'
 */
function getAll(firebaseAdmin, path) {
  return new Promise((resolve, reject) => {
    var db = firebaseAdmin.database();
    // the argument to ref is the "collection"
    // Get a database reference to theaters
    // set overwrites everything
    // update updates the IDs specified
    // push adds new data to the collection
    var dbRef = db.ref(path);
    // Attach an asynchronous callback to read the data at our posts reference
    dbRef.once(
      'value',
      function(snapshot) {
        // dbRef.off('value');
        resolve(snapshot.val());
      },
      function(errorObject) {
        // console.log(errorObject);
        // dbRef.off('value');
        resolve('The read failed: ' + errorObject.code);
      }
    );
  });
}

module.exports = getAll;
