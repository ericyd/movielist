/**
 * Save all data to Firebase for the path specified
 * @param {FirebaseAdmin} firebaseAdmin must already be initialized
 * @param {string} path either 'movies' or 'theaters'
 * @param {object} data example schemas listed at end of file
 */
function saveOne(firebaseAdmin, path, data) {
  var db = firebaseAdmin.database();
  // the argument to ref is the "collection"
  // Get a database reference to theaters
  // set overwrites everything
  // update updates the IDs specified
  // push adds new data to the collection
  var dbRef = db.ref(path);
  dbRef.push(data);
}

module.exports = saveOne;
