const htmlResponseWithoutBuyNow = `<div class='long-date'>times for <strong>Tuesday</strong>, June 6th, 2017</div><div class='theatre-header'>
                            <a href='https://www.google.com/maps/place/2735+East+Burnside+Street,+Portland,+OR+97214' target='blank' id='icon-map'><img src='/img/icoMap.png' class='fade' /></a>
                            <a href='http://www.exhibitorads.com/csentry/print.asp?house_id=2481&date=06-06-2017' target='blank' id='icon-print'><img src='/img/icoPrint.png' class='fade' /></a>
                            <a href='tel:503-232-5511' target='blank' id='icon-phone'><img src='/img/icoPhone.png' class='fade' /></a>
                            <span class='name'>Laurelhurst Theatre</span>
                            <span class='address'>2735 East Burnside Street, Portland, OR 97214 <span class='movieline'>&bull; 503-232-5511</span></span>
                         </div><div id='showtimes_movie' >
                                      <a href='/movie/238094/Colossal-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/238094H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/238094/Colossal-Trailer-and-Info' class='name'>Colossal <span class='mpaa'>(R)</span></a>
                                          <span class='actors'>Anne Hathaway, Jason Sudeikis, Dan Stevens, Austin Stowell  <span class='runtime'> &bull; 1 hr. 50 min.</span></span>
                                         <span class='comment'></span>
                                      <span class='times'><span class="showtime">9:30 pm</span></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/206627/Beauty-and-the-Beast-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/206627H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/206627/Beauty-and-the-Beast-Trailer-and-Info' class='name'>Beauty and the Beast <span class='mpaa'>(PG)</span></a>
                                          <span class='actors'>Emma Watson, Dan Stevens, Luke Evans, Emma Thompson  <span class='runtime'> &bull; 2 hr. 19 min.</span></span>
                                         <span class='comment'></span>
                                      <span class='times'><span class="showtime">8:45 pm</span></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/184127/Logan-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/184127H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/184127/Logan-Trailer-and-Info' class='name'>Logan <span class='mpaa'>(R)</span></a>
                                          <span class='actors'>Hugh Jackman, Patrick Stewart, Richard E. Grant, Boyd Holbrook  <span class='runtime'> &bull; 2 hr. 21 min.</span></span>
                                         <span class='comment'></span>
                                      <span class='times'><span class="showtime">9:00 pm</span></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/232345/Kedi-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/232345H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/232345/Kedi-Trailer-and-Info' class='name'>Kedi <span class='mpaa'>(NR)</span></a>
                                          <span class='actors'>BÃ¼lent ÃœstÃ¼n  <span class='runtime'> &bull; 1 hr. 20 min.</span></span>
                                         <span class='comment'></span>
                                      <span class='times'><span class="showtime">9:20 pm</span></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/209930/Lion-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/209930H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/209930/Lion-Trailer-and-Info' class='name'>Lion <span class='mpaa'>(PG-13)</span></a>
                                          <span class='actors'>Dev Patel, Rooney Mara, Nicole Kidman, David Wenham  <span class='runtime'> &bull; 2 hr. 0 min.</span></span>
                                         <span class='comment'></span>
                                      <span class='times'><span class="showtime">6:15 pm</span></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/5112/The-Big-Sleep-(1946)-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/005112H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/5112/The-Big-Sleep-(1946)-Trailer-and-Info' class='name'>The Big Sleep (1946) <span class='mpaa'>(NR)</span></a>
                                          <span class='actors'>Humphrey Bogart, Lauren Bacall, John Ridgely, Martha Vickers  <span class='runtime'> &bull; 1 hr. 56 min.</span></span>
                                         <span class='comment'></span>
                                      <span class='times'><span class="showtime">6:30 pm</span></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/1365/For-Your-Eyes-Only-Trailer-and-Info'>
                                         <img src='/img/missing.png' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/1365/For-Your-Eyes-Only-Trailer-and-Info' class='name'>For Your Eyes Only <span class='mpaa'>(PG)</span></a>
                                          <span class='actors'>Roger Moore  <span class='runtime'> &bull; 2 hr. 7 min.</span></span>
                                         <span class='comment'>No Passes Allowed</span>
                                      <span class='times'><span class="showtime">6:45 pm</span></span></div><div class='clearfix'></div></div>`;

const htmlResponseWithBuyNow = `<div class='long-date'>times for <strong>Tuesday</strong>, June 6th, 2017</div><div class='theatre-header'>
                            <a href='https://www.google.com/maps/place/4040+SE+82nd+Avenue,+Portland,+OR+97266' target='blank' id='icon-map'><img src='/img/icoMap.png' class='fade' /></a>
                            <a href='http://www.exhibitorads.com/csentry/print.asp?house_id=5316&date=06-06-2017' target='blank' id='icon-print'><img src='/img/icoPrint.png' class='fade' /></a>
                            <a href='tel:800-FAN-DANG' target='blank' id='icon-phone'><img src='/img/icoPhone.png' class='fade' /></a>
                            <span class='name'>Century Eastport 16</span>
                            <span class='address'>4040 SE 82nd Avenue, Portland, OR 97266 <span class='movieline'>&bull; 800-FAN-DANG</span></span>
                         </div><div id='showtimes_movie' >
                                      <a href='/movie/189708/Captain-Underpants-The-First-Epic-Movie-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/189708H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/189708/Captain-Underpants-The-First-Epic-Movie-Trailer-and-Info' class='name'>Captain Underpants: The First Epic Movie <span class='mpaa'>(PG)</span></a>
                                          <span class='actors'>Kevin Hart, Ed Helms, Nick Kroll, Thomas Middleditch  <span class='runtime'> &bull; 1 hr. 28 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=189708&perfd=06062017&perft=09:20" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:20 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189708&perfd=06062017&perft=11:40" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>11:40 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189708&perfd=06062017&perft=14:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>2:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189708&perfd=06062017&perft=15:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>3:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189708&perfd=06062017&perft=16:40" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>4:40 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189708&perfd=06062017&perft=19:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>7:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189708&perfd=06062017&perft=21:40" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:40 pm</a></span><span class='comment'>Luxury Lounger RealD 3D</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=189710&perfd=06062017&perft=10:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:30 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189710&perfd=06062017&perft=13:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>1:00 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189710&perfd=06062017&perft=18:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>6:00 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=189710&perfd=06062017&perft=20:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>8:30 pm</a></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/230592/Wonder-Woman-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/230592H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/230592/Wonder-Woman-Trailer-and-Info' class='name'>Wonder Woman <span class='mpaa'>(PG-13)</span></a>
                                          <span class='actors'>Gal Gadot, Chris Pine, Connie Nielsen, Robin Wright  <span class='runtime'> &bull; 2 hr. 21 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=09:20" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:20 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=10:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:10 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=12:45" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>12:45 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=13:40" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>1:40 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=16:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>4:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=17:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>5:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=18:40" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>6:40 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=19:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>7:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=20:40" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>8:40 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=230592&perfd=06062017&perft=22:55" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:55 pm</a></span><span class='comment'>Luxury Lounger RealD 3D</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=246412&perfd=06062017&perft=11:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>11:00 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=246412&perfd=06062017&perft=11:50" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>11:50 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=246412&perfd=06062017&perft=14:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>2:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=246412&perfd=06062017&perft=15:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>3:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=246412&perfd=06062017&perft=18:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>6:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=246412&perfd=06062017&perft=21:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=246412&perfd=06062017&perft=22:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:10 pm</a></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/156759/Pirates-of-the-Caribbean-Dead-Men-Tell-No-Tales-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/156759H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/156759/Pirates-of-the-Caribbean-Dead-Men-Tell-No-Tales-Trailer-and-Info' class='name'>Pirates of the Caribbean: Dead Men Tell No Tales <span class='mpaa'>(PG-13)</span></a>
                                          <span class='actors'>Johnny Depp, Kaya Scodelario, Orlando Bloom, Brenton Thwaites  <span class='runtime'> &bull; 2 hr. 15 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=09:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:30 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=10:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:00 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=10:45" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:45 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=12:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>12:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=13:15" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>1:15 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=14:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>2:00 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=15:45" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>3:45 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=16:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>4:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=17:15" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>5:15 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=19:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>7:00 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=19:45" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>7:45 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=20:25" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>8:25 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=22:15" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:15 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=156759&perfd=06062017&perft=23:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>11:00 pm</a></span><span class='comment'>Luxury Lounger RealD 3D</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=208392&perfd=06062017&perft=11:45" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>11:45 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=208392&perfd=06062017&perft=15:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>3:00 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=208392&perfd=06062017&perft=18:15" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>6:15 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=208392&perfd=06062017&perft=21:20" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:20 pm</a></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/225802/Baywatch-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/225802H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/225802/Baywatch-Trailer-and-Info' class='name'>Baywatch <span class='mpaa'>(R)</span></a>
                                          <span class='actors'>Dwayne Johnson, Zac Efron, Priyanka Chopra, Kelly Rohrbach  <span class='runtime'> &bull; 1 hr. 59 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=10:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:00 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=11:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>11:30 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=13:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>1:00 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=14:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>2:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=16:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>4:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=17:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>5:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=19:20" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>7:20 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=20:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>8:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=225802&perfd=06062017&perft=22:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:30 pm</a></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/249016/IN-OUR-HANDS-Battle-for-Jerusalem-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/249016H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/249016/IN-OUR-HANDS-Battle-for-Jerusalem-Trailer-and-Info' class='name'>IN OUR HANDS: Battle for Jerusalem <span class='mpaa'>()</span></a>
                                          <span class='actors'>Haim Almakis, Zion Ashkenazi, Assaf Ashtar, Idan Barkai  <span class='runtime'> &bull; 2 hr. 0 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=249016&perfd=06062017&perft=19:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>7:00 pm</a></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/222426/Alien-Covenant-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/222426H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/222426/Alien-Covenant-Trailer-and-Info' class='name'>Alien: Covenant <span class='mpaa'>(R)</span></a>
                                          <span class='actors'>Michael Fassbender, Katherine Waterston, Billy Crudup, Danny McBride  <span class='runtime'> &bull; 2 hr. 3 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=222426&perfd=06062017&perft=10:40" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:40 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=222426&perfd=06062017&perft=13:45" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>1:45 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=222426&perfd=06062017&perft=16:45" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>4:45 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=222426&perfd=06062017&perft=20:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>8:00 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=222426&perfd=06062017&perft=23:00" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>11:00 pm</a></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/241804/Everything-Everything-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/241804H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/241804/Everything-Everything-Trailer-and-Info' class='name'>Everything, Everything <span class='mpaa'>(PG-13)</span></a>
                                          <span class='actors'>Ana de la Reguera, Taylor Hickson, Nick Robinson, Amandla Stenberg  <span class='runtime'> &bull; 1 hr. 36 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=241804&perfd=06062017&perft=09:50" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:50 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=241804&perfd=06062017&perft=12:25" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>12:25 pm</a></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/220572/Snatched-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/220572H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/220572/Snatched-Trailer-and-Info' class='name'>Snatched <span class='mpaa'>(R)</span></a>
                                          <span class='actors'>Amy Schumer, Goldie Hawn, Joan Cusack, Ike Barinholtz  <span class='runtime'> &bull; 1 hr. 31 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=220572&perfd=06062017&perft=22:50" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:50 pm</a></span></div><div class='clearfix'></div></div><div id='showtimes_movie' >
                                      <a href='/movie/191861/Guardians-of-the-Galaxy-Vol.-2-Trailer-and-Info'>
                                         <img src='http://www.movienewsletters.net/photos/191861H1.jpg' class='poster fade' />
                                      </a>
                                      <div class='info'>
                                          <a href='/movie/191861/Guardians-of-the-Galaxy-Vol.-2-Trailer-and-Info' class='name'>Guardians of the Galaxy Vol. 2 <span class='mpaa'>(PG-13)</span></a>
                                          <span class='actors'>Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel  <span class='runtime'> &bull; 2 hr. 17 min.</span></span>
                                         <span class='comment'>Digital Cinema Luxury Lounger</span>
                                      <span class='times'><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=09:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:30 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=11:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>11:30 am</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=12:50" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>12:50 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=14:50" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>2:50 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=16:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>4:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=18:10" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>6:10 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=19:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>7:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=21:30" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>9:30 pm</a><a href="http://tix.mvtx.us?house_id=5316&movie_id=191861&perfd=06062017&perft=22:50" target="_blank" class="buy-tix fade"><span class="buy-tix-hover">Buy Now</span>10:50 pm</a></span></div><div class='clearfix'></div></div>`;

module.exports = {
  htmlResponseWithoutBuyNow: htmlResponseWithoutBuyNow,
  htmlResponseWithBuyNow: htmlResponseWithBuyNow
};
