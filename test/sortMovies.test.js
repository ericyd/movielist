const assert = require('chai').assert;
const processResults = require('../lib/processResults').transformData;
const parseTheaterHTML = require('../lib/util/parseTheaterHTML');
const mockResponse = require('./mock').htmlResponseWithoutBuyNow;
const sortObjectsByProp = require('../src/util/sortObjectsByProp');

const movies = [
  {
    title: 'wonder woman',
    times: [],
    runtime: 'test'
  },
  {
    title: 'alvin and the chipmunks',
    times: [],
    runtime: 'test'
  },
  {
    title: 'george',
    times: [],
    runtime: 'test'
  }
];

describe('sortMovies', function() {
  it('should sort movies based on movie.title', function() {
    const sortedMovies = movies.sort(sortObjectsByProp('title'));
    // console.log(sortedMovies)
    // const titles = sortedMovies
    // .map(obj => {
    //   return Object.keys(obj)
    //     .filter(key => {
    //       return key === 'title';
    //     })
    //     .map(title => obj['title'])
    // });
    assert.equal(sortedMovies[0].title, 'alvin and the chipmunks')
    assert.equal(sortedMovies[1].title, 'george')
    assert.equal(sortedMovies[2].title, 'wonder woman')
  });
});
