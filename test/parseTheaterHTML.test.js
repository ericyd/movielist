const assert = require('chai').assert;
const parseTheaterHTML = require('../lib/util/parseTheaterHTML');
const mock = require('./mock');
const htmlResponseWithBuyNow = mock.htmlResponseWithBuyNow;
const htmlResponseWithoutBuyNow = mock.htmlResponseWithoutBuyNow;
// helper functions
const isNotNil = value => value !== null && value !== undefined;
const isNil = value => !isNotNil(value);

describe('parseTheaterHTML', function() {
  describe('Responses with "Buy Now" links', function() {
    const response = htmlResponseWithBuyNow;
    it('should return an object', function() {
      const typeOfResponse = typeof parseTheaterHTML(response);
      assert.equal(typeOfResponse, 'object', 'parsed result was not an object');
    });

    const stringProps = ['theaterName', 'rawHTML'];
    stringProps.forEach(prop => {
      it(`result should have a string "${prop}" property`, function() {
        const parsed = parseTheaterHTML(response);
        assert.isTrue(isNotNil(parsed[prop]), `${prop} was undefined or null`);
        assert.equal(typeof parsed[prop], 'string', `${prop} is not a string`);
      });
    });

    it('result should have a date "dateModified" property', function() {
      const parsed = parseTheaterHTML(response);
      assert.isTrue(
        isNotNil(parsed.dateModified),
        'dateModified was undefined or null'
      );
      // assert.equal(typeof parsed.dateModified, 'date');
    });

    it('result should have an array "movies" property', function() {
      const parsed = parseTheaterHTML(response);
      assert.isTrue(isNotNil(parsed.movies), 'movies was undefined or null');
      assert.isTrue(Array.isArray(parsed.movies), 'movies is not an array');
    });

    it('movie items should have an array "times" property', function() {
      const parsed = parseTheaterHTML(response);
      const movies = parsed.movies[0];
      assert.isTrue(
        isNotNil(movies.times),
        'movies.times was undefined or null'
      );
      assert.isTrue(
        Array.isArray(movies.times),
        'movies.times is not an array'
      );
    });

    it('movie items times should contain strings', function() {
      const parsed = parseTheaterHTML(response);
      const movieTime = parsed.movies[0].times[0];
      assert.isTrue(isNotNil(movieTime), 'movieTime was undefined or null');
      assert.equal(typeof movieTime, 'string', 'movieTime is not a string');
    });

    const movieStringProps = ['title', 'runtime', 'link', 'imageSrc'];
    movieStringProps.forEach(prop => {
      it(`movie items should have string "${prop}" property`, function() {
        const parsed = parseTheaterHTML(response);
        const movies = parsed.movies[0];
        assert.isTrue(
          isNotNil(movies[prop]),
          `movies.${prop} was undefined or null`
        );
        assert.equal(
          typeof movies[prop],
          'string',
          `movies.${prop} is not a string`
        );
      });
    });
  });

  describe('Responses with "Buy Now" links', function() {
    const response = htmlResponseWithoutBuyNow;
    it('should return an object', function() {
      const typeOfResponse = typeof parseTheaterHTML(response);
      assert.equal(typeOfResponse, 'object', 'parsed result was not an object');
    });

    const stringProps = ['theaterName', 'rawHTML'];
    stringProps.forEach(prop => {
      it(`result should have a string "${prop}" property`, function() {
        const parsed = parseTheaterHTML(response);
        assert.isTrue(isNotNil(parsed[prop]), `${prop} was undefined or null`);
        assert.equal(typeof parsed[prop], 'string', `${prop} is not a string`);
      });
    });

    it('result should have a date "dateModified" property', function() {
      const parsed = parseTheaterHTML(response);
      assert.isTrue(
        isNotNil(parsed.dateModified),
        'dateModified was undefined or null'
      );
      // assert.equal(typeof parsed.dateModified, 'date');
    });

    it('result should have an array "movies" property', function() {
      const parsed = parseTheaterHTML(response);
      assert.isTrue(isNotNil(parsed.movies), 'movies was undefined or null');
      assert.isTrue(Array.isArray(parsed.movies), 'movies is not an array');
    });

    it('movie items should have an array "times" property', function() {
      const parsed = parseTheaterHTML(response);
      const movies = parsed.movies[0];
      assert.isTrue(
        isNotNil(movies.times),
        'movies.times was undefined or null'
      );
      assert.isTrue(
        Array.isArray(movies.times),
        'movies.times is not an array'
      );
    });

    it('movie items times should contain strings', function() {
      const parsed = parseTheaterHTML(response);
      const movieTime = parsed.movies[0].times[0];
      assert.isTrue(isNotNil(movieTime), 'movieTime was undefined or null');
      assert.equal(typeof movieTime, 'string', 'movieTime is not a string');
    });

    const movieStringProps = ['title', 'runtime', 'link', 'imageSrc'];
    movieStringProps.forEach(prop => {
      it(`movie items should have string "${prop}" property`, function() {
        const parsed = parseTheaterHTML(response);
        const movies = parsed.movies[0];
        assert.isTrue(
          isNotNil(movies[prop]),
          `movies.${prop} was undefined or null`
        );
        assert.equal(
          typeof movies[prop],
          'string',
          `movies.${prop} is not a string`
        );
      });
    });
  });
});
