const assert = require('chai').assert;
const processResults = require('../lib/processResults');
const parseTheaterHTML = require('../lib/util/parseTheaterHTML');
const getAll = require('../models/getAll');
const mockResponse = require('./mock').htmlResponseWithoutBuyNow;

// this is all the methods to test from processResults
// module.exports = {
//   transformData: processResults,
//   getMovieDetails: getMovieDetails,
//   getAllShowtimesForMovie: getAllShowtimesForMovie,
//   getTheaterShowtimesForMovie: getTheaterShowtimesForMovie,
//   isMovieShowingAtTheater: isMovieShowingAtTheater,
//   getUniqueMovies: getUniqueMovies
// };

describe('processResults', function() {
  describe.skip('transformData', function() {
    it('should start with an array of Theater schemas and end with an array of Movie schemas', function() {});
  });

  describe.skip('getTheaterShowtimesForMovie', function() {
    const getTheaterShowtimesForMovie =
      processResults.getTheaterShowtimesForMovie;

    it('should return an array', function() {});

    it('each array item should have a theater property', function() {});

    it('each array item should have a times array property', function() {});
  });

  describe('isMovieShowingAtTheater', function() {
    const isMovieShowingAtTheater = processResults.isMovieShowingAtTheater;

    it('should return true if the movie is in the theater movie list', function() {
      const theater = parseTheaterHTML(mockResponse);
      const isShowing = isMovieShowingAtTheater(
        theater,
        theater.movies[0].title
      );
      assert.isTrue(
        isShowing,
        'movie should be showing at theater but was not found'
      );
    });

    it('should return false if movie is not in the theater movie list', function() {
      const theater = parseTheaterHTML(mockResponse);
      const isShowing = isMovieShowingAtTheater(theater, 'bogus movie title');
      assert.isFalse(
        isShowing,
        'movie should not be showing at theater but was found'
      );
    });

    it('should return false if theater is not the right schema', function() {
      const theater = 42;
      let isShowing = isMovieShowingAtTheater(theater, 'bogus movie title');
      assert.isFalse(
        isShowing,
        'theater is a number, should not have movies playing'
      );
      isShowing = isMovieShowingAtTheater({ movies: 42 }, 'bogus movie title');
      assert.isFalse(
        isShowing,
        'theater.movies is not an array should not be showing at theater but was found'
      );
    });
  });

  // This test suite passes, but requires db connectivity
  // it was written to verify results
  describe.skip('getMovieDetails', function() {
    require('dotenv').config();
    const firebase = require('firebase');
    //
    // Initialize Firebase
    //
    const firebaseAdmin = firebase.initializeApp({
      apiKey: process.env.API_KEY,
      authDomain: process.env.AUTH_DOMAIN,
      databaseURL: process.env.FIREBASE_URL,
      storageBucket: process.env.STORAGE_BUCKET,
      projectId: process.env.PROJECT_ID,
      messagingSenderId: process.env.MESSAGING_SENDER_ID
    });

    // sign in so data can be saved to the db
    const auth = firebaseAdmin.auth();
    auth
      .signInWithEmailAndPassword(process.env.EMAIL, process.env.PASSWORD)
      .then(user => {
        // console.log(auth.currentUser);
      })
      .catch(err => {
        console.log(err);
      });
    it('should get the movie details for the supplied movieTitle', function(
      done
    ) {
      this.timeout(20000);
      getAll(firebaseAdmin, 'theaters').then(theaters => {
        const movieTitle = theaters[0].movies[0].title;
        // console.log(movieTitle);
        const showtimes = processResults.getAllShowtimesForMovie(
          theaters,
          movieTitle
        );
        // console.log(showtimes);
        const movieDetails = processResults.getMovieDetails(
          theaters,
          showtimes,
          movieTitle
        );
        // console.log(movieDetails);

        assert.equal(
          typeof movieDetails.runtime,
          'string',
          'runtime is not a string'
        );
        assert.equal(
          typeof movieDetails.link,
          'string',
          'link is not a string'
        );
        assert.equal(
          typeof movieDetails.imageSrc,
          'string',
          'imageSrc is not a string'
        );

        // link and imageSrc should be fully qualified URLs
        assert.notEqual(movieDetails.link.indexOf('http://'), -1);
        assert.notEqual(movieDetails.imageSrc.indexOf('http://'), -1);

        done();
      });
    });
  });
});
