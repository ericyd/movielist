// const firebase = require('firebase');

// const app = firebase.initializeApp({
//   apiKey: process.env.API_KEY,
//   authDomain: process.env.AUTH_DOMAIN,
//   databaseURL: process.env.FIREBASE_URL,
//   storageBucket: process.env.STORAGE_BUCKET,
//   projectId: process.env.PROJECT_ID,
//   messagingSenderId: process.env.MESSAGING_SENDER_ID
// });

require('dotenv').config();
var admin = require('firebase-admin');

var serviceAccount = require('../../secret-firebase-key.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_URL
});

var db = admin.database();
// the argument to ref is the "collection"
// Get a database reference to theaters
// set overwrites everything
// update updates the IDs specified
// push adds new data to the collection
var theatersRef = db.ref('theaters');
theatersRef.push({
  theaterName: 'test theater name',
  rawHTML: '<html></html>',
  dateModified: new Date(),
  movies: [
    {
      title: 'test movie title',
      imageSrc: 'http://www.test-image.com',
      link: 'http://www.google.com',
      runtime: '2hrs',
      times: ['2:05 pm', '4:55 pm']
    }
  ]
});
