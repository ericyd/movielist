// This test suite passes, but requires db connectivity
// it was written to verify results
require('dotenv').config();
const firebase = require('firebase');
const getOne = require('../../models/getOne');
const saveOne = require('../../models/saveOne');
//
// Initialize Firebase
//
const firebaseAdmin = firebase.initializeApp({
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_URL,
  storageBucket: process.env.STORAGE_BUCKET,
  projectId: process.env.PROJECT_ID,
  messagingSenderId: process.env.MESSAGING_SENDER_ID
});

// sign in so data can be saved to the db
const auth = firebaseAdmin.auth();
auth
  .signInWithEmailAndPassword(process.env.EMAIL, process.env.PASSWORD)
  .then(user => {
    return saveOne(firebaseAdmin, 'lastUpdated', {
      timestamp: new Date().getTime()
    });
  })
  .then(done => {
    // console.log(done.val());
    return getOne(firebaseAdmin, 'lastUpdated');
  })
  .then(response => {
    const data = response[Object.keys(response)[0]];
    console.log(new Date(data.timestamp));
    // const movieTitle = theaters[0].movies[0].title;
    // // console.log(movieTitle);
    // const showtimes = processResults.getAllShowtimesForMovie(theaters, movieTitle);
    // // console.log(showtimes);
    // const movieDetails = processResults.getMovieDetails(theaters, showtimes, movieTitle);
    // // console.log(movieDetails);
  })
  .catch(console.log.bind(console));
