require('dotenv').config();
const admin = require('firebase-admin');
const serviceAccount = require('../../secret-firebase-key.json');
const getAll = require('../../models/getAll');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_URL
});

getAll(admin, 'theaters')
  .then(theaters => {
    console.log(theaters);
  })
  .catch(err => {
    console.log('error getting data', err);
  });
