const assert = require('chai').assert;
const promisifyArray = require('../lib/util/promisifyArray');
const promiseFunc = () => {
  return new Promise((res, rej) => res('resolved'));
};

describe('promisifyArray', function() {
  it("should throw an error if first arg isn't an array", function() {
    try {
      promisifyArray('string as first arg');
    } catch (err) {
      assert.equal(
        err.message,
        'first argument is not an array',
        'error not thrown or message did not match'
      );
    }
  });

  it('should return an array of promises', function() {
    const promiseArray = promisifyArray([1, 2, 3], promiseFunc);
    assert.isTrue(Array.isArray(promiseArray), 'promiseArray is not an array');
    promiseArray.forEach((promise, i) => {
      assert.isTrue(
        promiseArray[i] instanceof Promise,
        `promiseArray[${i}] is not an instance of Promise`
      );
    });
  });
});
